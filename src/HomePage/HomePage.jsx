import { Link } from "react-router-dom";
import Select from "react-select";
import React, { useState } from "react";
import { Button } from "react-bootstrap-buttons";
import Modal from "react-modal";
import "./custom.css";

class HomePage extends React.Component {
  render() {
    return (
      <div className="col-md-6 col-md-offset-3">
        <h1>Hi admin!</h1>
        <div>
          <AppChoice />
        </div>
        <p>
          <Link to="/login">Logout</Link>
        </p>
      </div>
    );
  }
}

function AppChoice() {
  var NotBegunGames = [];
  var activeGames = [];

  const [selectedGames, setSelectedGames] = useState([]);
  const [selectedGamesFinish, setSelectedGamesFinish] = useState([]);

  const [isOpenGame, setIsOpenGame] = useState(false);
  const [isOpenCheckpoint, setIsOpenCheckpoint] = useState(false);

  function toggleModalGame() {
    setIsOpenGame(!isOpenGame);
  }

  function toggleModalCheckpoint() {
    setIsOpenCheckpoint(!isOpenCheckpoint);
  }

  getNotBegunGames();
  getActiveGames();

  function getNotBegunGames() {
    NotBegunGames = [];
    fetch("https://iot-virumaa-backend.herokuapp.com/getNotBegunGames")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        var dataFound = JSON.parse(data);
        dataFound.forEach((element) => {
          NotBegunGames.push({ label: element.title, value: element.id });
        });
      });
  }

  function getActiveGames() {
    activeGames = [];
    fetch("https://iot-virumaa-backend.herokuapp.com/getActiveGames")
        .then((response) => {
          return response.text();
        })
        .then((data) => {
          var dataFound = JSON.parse(data);
          dataFound.forEach((element) => {
            activeGames.push({ label: element.title, value: element.id });
          });
        });
  }

  const titleRef = React.useRef();
  const descriptionRef = React.useRef();
  const countryRef = React.useRef();
  const placeRef = React.useRef();
  const date_gameRef = React.useRef();
  const time_beginRef = React.useRef();
  const mapRef = React.useRef();
  const distanceRef = React.useRef();

  const checkpoint_idRef = React.useRef();
  const orderRef = React.useRef();
  const longitudeRef = React.useRef();
  const latitudeRef = React.useRef();

  const handleSubmitGame = () => {
    fetch("https://iot-virumaa-backend.herokuapp.com/games", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
      body: JSON.stringify({
        title: titleRef.current.value,
        description: descriptionRef.current.value,
        country: countryRef.current.value,
        place: placeRef.current.value,
        date_game: date_gameRef.current.value,
        time_begin: time_beginRef.current.value,
        map: mapRef.current.value,
        distance: distanceRef.current.value,
        status: "not_begun",
      }),
    });
    getNotBegunGames();
    toggleModalGame();
  };

  const handleSubmitCheckpoint = () => {
    fetch("https://iot-virumaa-backend.herokuapp.com/checkpoints", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
      body: JSON.stringify({
        game_id: selectedGames.value,
        checkpoint_id: checkpoint_idRef.current.value,
        order: orderRef.current.value,
        longitude: longitudeRef.current.value,
        latitude: latitudeRef.current.value,
      }),
    });
    toggleModalCheckpoint();
  };

  const startGame = () => {
     fetch("https://iot-virumaa-backend.herokuapp.com/setActive", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
      body: JSON.stringify({
        id: selectedGames.value,
      }),
    }).then(
    setTimeout(() => {
      getNotBegunGames(),
          getActiveGames(),
          window.location.reload(false)
    }, 1000),
  );
  };

  const finishGames = () => {
     fetch("https://iot-virumaa-backend.herokuapp.com/finish", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
      body: JSON.stringify({
        game_id: selectedGamesFinish.value,
      }),
     }).then(
         setTimeout(() => {
           getNotBegunGames(),
               getActiveGames(),
           window.location.reload(false)
         }, 1000),
  );
  };

  return (
    <div>
      <div>
        <h2>Game control panel</h2>
      </div>

      <Select
          options={NotBegunGames}
          onChange={setSelectedGames}
          labelledBy={"Select"}
      />
      <button className="btn btn-primary" onClick={startGame}>
        Start game
      </button>

      <p></p>

      <Select
          options={activeGames}
          onChange={setSelectedGamesFinish}
          labelledBy={"Select"}
      />
      <button className="btn btn-primary" onClick={finishGames}>
        Finish game
      </button>

      <p></p>

      <button className="btn btn-primary" onClick={toggleModalGame}>
        Add new game
      </button>
      <Modal
          className={["custom-nav-bg", "col-md-6 col-md-offset-3"].join(' ')}
        isOpen={isOpenGame}
        onRequestClose={toggleModalGame}
        contentLabel="New Game"
      >
        <div>
          <h1>Add new game</h1>
          <form>
            <p>Title</p>{" "}
            <input
              className="form-control"
              ref={titleRef}
              type="text"
              name="title"
            />
            <p>Description</p>{" "}
            <input
              className="form-control"
              ref={descriptionRef}
              type="text"
              name="description"
            />
            <p>Country</p>{" "}
            <input
              className="form-control"
              ref={countryRef}
              type="text"
              name="country"
            />
            <p>Place</p>{" "}
            <input
              className="form-control"
              ref={placeRef}
              type="text"
              name="place"
            />
            <p>Date game</p>{" "}
            <input
              className="form-control"
              ref={date_gameRef}
              type="text"
              name="date_game"
            />
            <p>Time begin</p>{" "}
            <input
              className="form-control"
              ref={time_beginRef}
              type="text"
              name="time_begin"
            />
            <p>Map</p>{" "}
            <input
              className="form-control"
              ref={mapRef}
              type="text"
              name="map"
            />
            <p>Distance</p>{" "}
            <input
              className="form-control"
              ref={distanceRef}
              type="text"
              name="distance"
            />
            <p></p>
            <input
              className="btn btn-primary"
              type="button"
              value="Add new game"
              onClick={handleSubmitGame}
            />
            <input
              className="btn btn-primary"
              type="button"
              value="Back"
              onClick={toggleModalGame}
            />
            <p></p>
          </form>
        </div>
      </Modal>

      <button className="btn btn-primary" onClick={toggleModalCheckpoint}>
        Add new checkpoint
      </button>
      <Modal
          className={["custom-nav-bg", "col-md-6 col-md-offset-3"].join(' ')}
        isOpen={isOpenCheckpoint}
        onRequestClose={toggleModalCheckpoint}
        contentLabel="New Checkpoint"
      >
        <div>
          <h1>Add new checkpoint</h1>
          <form>
            <p>Game</p>
            <Select
              options={NotBegunGames}
              onChange={setSelectedGames}
              labelledBy={"Select"}
            />
            <p>Checkpoint id</p>{" "}
            <input
              className="form-control"
              ref={checkpoint_idRef}
              type="text"
              name="checkpoin_id"
            />
            <p>Order</p>{" "}
            <input
              className="form-control"
              ref={orderRef}
              type="text"
              name="order"
            />
            <p>Longitude</p>{" "}
            <input
              className="form-control"
              ref={longitudeRef}
              type="text"
              name="longitude"
            />
            <p>Latitude</p>{" "}
            <input
              className="form-control"
              ref={latitudeRef}
              type="text"
              name="latitude"
            />
            <p></p>
            <input
              className="btn btn-primary"
              type="button"
              value="Add new checkpoint"
              onClick={handleSubmitCheckpoint}
            />
            <input
              className="btn btn-primary"
              type="button"
              value="Back"
              onClick={toggleModalCheckpoint}
            />
            <p></p>
          </form>
        </div>
      </Modal>
      <p></p>
      <p></p>
    </div>
  );
}

export { HomePage };
