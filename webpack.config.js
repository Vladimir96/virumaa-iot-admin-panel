var HtmlWebpackPlugin = require('html-webpack-plugin');
var server_port = process.env.YOUR_PORT || process.env.PORT || 5000;
var server_host = process.env.YOUR_HOST || "0.0.0.0";

module.exports = {
    resolve: {
        extensions: ['.js', '.jsx', '.css']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader'
            },
            { test: /\.css$/, loader: "style-loader!css-loader" }
        ]
    },
    devServer: {
        disableHostCheck: true,
        compress: false,
        inline: true,
        port: server_port,
        host: server_host

    },
    plugins: [new HtmlWebpackPlugin({
        template: './src/index.html',
    })]
};